using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: CLSCompliantAttribute(false)]
[assembly: AssemblyTitleAttribute("MSNPSharp MSN Protocol Library")]
[assembly: AssemblyDescriptionAttribute("C# implementation of the MSN protocol")]
[assembly: AssemblyCopyrightAttribute("Copyright (c) 2002-2012, Bas Geertsema, Xih Solutions (http://www.xihsolutions.net), Thiago.Sayao, Pang Wu, Ethem Evlice, Andy Phan, Chang Liu. ")]
[assembly: AssemblyProductAttribute("MSNPSharp")]
[assembly: AssemblyDelaySignAttribute(false)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisibleAttribute(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: GuidAttribute("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC")]

// Version information for an assembly consists of the following four values:
// Major.Minor.Build.SVNRevision
[assembly: AssemblyVersionAttribute("5.0.0.2714")]
[assembly: AssemblyFileVersionAttribute("5.0.0.2714")]
[assembly: AssemblyCompanyAttribute("MSNPSharp")]
[assembly: AssemblyTrademarkAttribute("MSNPSharp")]
